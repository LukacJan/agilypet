AgilyPet

Spletna aplikacija za ustvarjanje agility poligonov za pse in spremnljanje agility dogodkov.

Leto izdelave: 2022

Jan Lukač
Blazhe Manev
Gregor Pojbič
Kristina Ristova

Domača stran: / (Stran deluje le na localhost:3000)

Koda: https://gitlab.com/LukacJan/agilypet.git

Kategorizacija: spletna aplikacija

Oznake: ReactJS, MongoDB, NodeJS, AgilenRazvoj, GitLab, Express, Docker.

Opis projekta v nekaj odstavkih / vrsticah (cca 150 besed)

Spletna aplikacija AgilyPet registriranim uporabnik omogoča vnašanje informacij o svojih psih, na podlagi katerih lahko 
najdejo ustrezne poligone za svoje pse. Na razpolago imajo tudi pridobivanje osnovnih informacij o raznih pasmah psov. Lahko izbirajo 
med vsemi pasmami, ali pa le med pasmami svojih psov.
Prav tako lahko ustvarijo in objavijo svoj poligon, katerega si ogledajo drugi uporabniki.
Pri ustvarjanju lahko označijo, če je poligon primeren za pse z določenimi zdravstvenimi težavami. Med brskanjem po poligonih, se lahko 
uporabniki odločijo "slediti" drugim ustvarjalcem poligonov. Tako bodo preko email-a obveščeni, ko uporabnik, ki mu sledijo, naloži nov poligon.
Uporabniki prav tako lahko uporabijo koledar za sledenju dogodkov povezanimi z agility, kot so tekmovanja in treningi. Dogodke lahko dodajo kar sami, 
lahko pa se prijavijo na že objavljene dogodke. Vsi na novo dodani dogodki se avtomatsko vnesejo v koledar.
Koledar dogodkov in seznam poligonov sta na voljo za ogled tudi neregistriranim uporabnikom.